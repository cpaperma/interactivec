function ic_magic
	set parse (string split ' ' $argv[2])
	set command $parse[1]
	set argument $parse[2]
	switch $command 
	case reset
		if test "$argument" = "functions"
			echo -n > $ic_functions
		else
			reset
		end
	case replay
		set -g outputnb 0
		ic_exec
	case show
		switch "$argument"
		case ""
			cat -n $ic_body
		case "body"
			cat -n $ic_body
		case source
			ic_assemble
		case header
			cat -n $ic_header
		case functions
			cat -n $ic_function
		end
	case delete
		if ! test "$argument" = ""
			set lnb $argument
		else
			set lnb 1
		end
		sed -i $lnb"d" $ic_body
	case save
		if test "argument" = "source"
			ic_assemble > $parse[3]
		else
			cat $ic_body > $argument
		end
	case replay
		set -g outputnb 0
		ic_exec
	case edit
		switch "$argument"
		case functions
			set ledit $ic_functions
		case header
			set ledit $ic_header
		case body
			set ledit $ic_body
		case restore
			cat $ic_restore > $ic_body
			set ledit $ic_body
		case ""
			set ledit $ic_body
		case "*"
			set ledit $argument
		end
		editor $ledit
		set -g outputnb 0
		ic_exec
	case option
		switch "$argument"
		case add
			set -g  ic_compile_option $parse[3] $ic_compile_option
		case reset
			set -g ic_compile_option ""
		end

	case help
		echo magic commands start by %:
		echo %reset: reset the whole computation
		echo %show: [source, body, function, header]: show the inputed commands [source code, functions or header]
		echo %edit: [body, functions, header, restore] open system editor to edit inputed lines [functions, header] and restart computations.
		echo -e "\t "restore" restore the last version of body before execution. Can be useful to reedit non executable previous source code."
		echo %delete: [n]: delete the last input [the nth input with n the line number provided by %show]
		echo %replay: replay the whole computation from scratch.
		echo %save: [source] file: save inputed lines [whole source]to file.
		echo "!cmd" execute "eval cmd".
		echo %help: display this help
	case '*'
		echo "$command" is not a known magic command. Type %help for the help.
	end
end	
