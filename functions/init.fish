function ic_init
	set -g tmpdir (mktemp -d)
	set -g ic_template_dir "$ic_path/templates/"(ls "$ic_path/templates/" | grep -xi "$ic_lang")
	source $ic_template_dir/configure.fish
	set -g ic_template_file (ls $ic_template_dir/template*)
	set -g ic_extension (string split "." $ic_template_file)[-1]
	if set -q ic_follow
		set -g ic_body "$ic_follow"
	else
		set -g ic_body "$tmpdir/body.$ic_extension"
	end
	if ! test -e $ic_body
		echo -n > $ic_body
	end
	set -g ic_functions "$tmpdir/functions.$ic_extension"
	set -g ic_header "$tmpdir/header.$ic_extension"
	set -g ic_restore "$tmpdir/restore.$ic_extension"
	set -g ic_output "$tmpdir/output"
	cat $ic_template_dir/header* > $ic_header
	set -g ginputnb 1
	set -g ic_output_bin $tmpdir/bin
	set -g ic_source "$tmpdir/source.$ic_extension"
	source $ic_path/welcome
	reset
end

function reset
	echo -n > $ic_functions
	echo -n > $ic_output
	set -g outputnb 0
	if set -q ic_include_body
		cat $ic_include_body >> $ic_body
	end
	if set -q ic_include_header
		cat $ic_include_header >> $ic_header
	end
	if set -q ic_include_function
		cat $ic_include_function >> $ic_function
	end
end


