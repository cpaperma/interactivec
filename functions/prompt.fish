function ic_prompt
	set_color -o $fish_color_cwd
	echo -n "> "$ic_lang"["
	set_color brcyan
	echo -n $ginputnb
	set_color -o $fish_color_cwd
	echo -n "]"
	set_color normal
	echo -n ": "
end
