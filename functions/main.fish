function ic_main 
	if test (count $argv) =  1
		set -g ic_lang (ls $ic_path/templates | grep -i $argv[1])
	else
		set -g ic_lang "$ic_default_lang"
	end
	ic_init
	if ! test (wc -l $ic_body | cut -f1 -d" ") = 0
		echo "Replay:"
		echo
		ic_exec
		echo 
	end
	trap "rm -r $tmpdir" EXIT
	while read -p ic_prompt -l line
		set -g ic_code_save (cat $ic_body)
		if test (string trim $line) = ""
			continue
		end
		set magicarg (string match -r '^%(.*)' $line)
		if count $magicarg > /dev/null
			ic_magic $magicarg		
			continue
		end
		set escape (string match -r '^!(.*)' $line)
		if count $escape > /dev/null 
			eval $escape[2]
			continue
		end
		echo $line >> $ic_body
		ic_exec
	end
	rm -r $tmpdir
end
