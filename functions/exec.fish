function ic_assemble
	cat $ic_template_file | $ic_default_template_engine header=$ic_header functions=$ic_functions body=$ic_body 
end
function ic_exec
	cat $ic_body > $ic_restore
	set ginputnb (expr $ginputnb + 1)
	ic_assemble > $ic_source
	set success "0"
	ic_compile_cmd 
	if test "$status" = "0"
		ic_execute_cmd > $ic_output
	else
		echo $ic_code_save > $ic_body
	end
	set noutputnb (cat $ic_output | wc -l)
	tail -n(expr $noutputnb - $outputnb) $ic_output | display
	set outputnb $noutputnb 
end

function display
	set outmess "   Out: "
	while read -l curr
		set_color brred
		echo -n "$outmess"
		set outmess "   ...: " 
		set_color normal
		echo $curr
		echo $curr > $ic_output
	end
end
