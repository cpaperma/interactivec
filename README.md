# Interactive C(ompile)

An incremental lightweight fish based general purpose interactive command line tools for compiled languages. 

The code use [fish shell](https://fishshell.com/) to provides an interactive prompt
that re-compile at each user input and output only what is new. It allows to test quickly
some code snippet without having to setup a skeleton to trash immediately.

The script uses templates to compile which can be easily edited, either permanently
or within a session with the %edit magic command.

The interactive prompt is largely inspire of standard functionalities of [ipython](https://ipython.org/). 

Usage: `./iC [Language]` will launch the interactive console. `iC` is equivalent to `ic C`. 
For another language, (e.g Java, assuming the template is installed), the command `iC Java` 
will launch  the interactive console for the Java template.

## Installation

The script work out of the box as long as dependencies are satisfied.
A full install from the current directory should be:

```
sudo apt install fish, python3, gcc
sudo ./install
```

The install script itself will just be copy of the current directory in `/etc/interactivec`
and of the script in `/usr/bin/`.

To remove the program, you can run 

```
sudo ./uninstall
```

## Dependencies

* fish (version > 3.02 works, version < 2.7 does not work).
* python3 (or replace the `default_template_engine`)
* gcc (for `iC C`)
